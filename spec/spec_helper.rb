require "bundler/setup"
require "request_log_analyzer"

Dir[File.dirname(__FILE__) + '/helpers/**/*.rb'].each do |file|
  require file
end

RSpec.configure do |config|
  config.include Helpers
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
