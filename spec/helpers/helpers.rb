module Helpers

  # Open a log file for read
  def log_file(name, extention = 'log', mode = 'r')
    File.open log_fixture(name, extention = 'log'), mode
  end

  # Load a log file from the fixture folder
  def log_fixture(name, extention = 'log')
    File.dirname(__FILE__) + "/../fixtures/#{name}.#{extention}"
  end
end
