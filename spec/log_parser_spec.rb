require "spec_helper"

RSpec.describe RequestLogAnalyzer::LogParser do

  subject { described_class.new(file_path) }

  describe '#perform' do
    context 'when specific log file provided' do
      let(:file_path) { log_fixture(:sample) }

      before { subject.perform }

      it 'stores data in request_info_store' do
        expect(subject.request_info_store["GET /api/users/{user_id}/count_pending_messages"][:hits_count]).to eq(2430)
        expect(subject.request_info_store["GET /api/users/{user_id}/count_pending_messages"][:median]).to eq(15.0)
        expect(subject.request_info_store["GET /api/users/{user_id}/count_pending_messages"][:mean]).to eq(25.99670781893004)
        expect(subject.request_info_store["GET /api/users/{user_id}/count_pending_messages"][:mode]).to eq([11])
        expect(subject.request_info_store["GET /api/users/{user_id}/count_pending_messages"][:top_dyno]).to eq("web.2")
      end
    end

    context 'when can not open file' do
      let(:file_path) { "not_existing_dir/not-existing-folder"}

      it 'puts to stdout information about that' do
        expect { subject.perform }.to output("Can\'t open this file: #{file_path}\n").to_stdout
      end
    end
  end
end
