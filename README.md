# RequestLogAnalyzer

Parse a log file and do some analysis on it.

## Usage

    $ bundle exec bin/request_log_analyzer analyze spec/fixtures/sample.log

## Running specs

    $ rspec spec

## TODO

- Current statistics data store is hash (hash is stored in memory). Consider use Redis to optimize memory usage.
- Better specs

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'request_log_analyzer', :bitbucket => 'rtrojanowski/request_log_analyzer'
```

And then execute:

    $ bundle

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/request_log_analyzer.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
