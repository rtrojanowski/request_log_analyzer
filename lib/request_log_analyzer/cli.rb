require 'thor'

module RequestLogAnalyzer
  class Cli < Thor
    desc "analyze path_to_log_file", "Parse a log file and do some analysis on it."

    def analyze(file_path)
      log_parser = RequestLogAnalyzer::LogParser.new(file_path)
      log_parser.perform
      log_parser.render_statistics
    end
  end
end
