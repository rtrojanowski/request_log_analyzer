require 'simple_stats'

module RequestLogAnalyzer
  class LogParser
    HEROKU_INFO_LOGS_REGEXP = /(\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d(?:\.\d\d\d\d\d\d)?\+00:00) ([a-z0-9\[\]:]+) at=([a-z]+) method=([A-Z]+) path=(\/[\S]+) host=([a-z.]+) fwd="([0-9.]+)" dyno=([a-z0-9.]+) connect=([\d]+)ms service=([\d]+)ms status=([\d]+) bytes=([\d]+)/
    # TODO: consider to improve regexp according to:
    # https://devcenter.heroku.com/articles/http-routing#heroku-router-log-format

    attr_reader :request_info_store

    def initialize(file_path)
      @file_path = file_path
      @request_info_store = {}
      initialize_store
    end

    def perform
      build_store
      add_statistics(@request_info_store)
    end

    def render_statistics
      @request_info_store.each_with_index do |(url ,x), index|
        puts url
        hash = x.reject {|key, value| [:dynos, :response_time].include?(key) }
        hash.each { |k, v| puts "#{k.to_s.ljust(20)} #{v}" }
        puts ""
      end
    end

    def add_statistics(data)
      data.each do |_, value|
        value[:median] = value[:response_time].median
        value[:mean] = value[:response_time].mean
        value[:mode] = value[:response_time].modes
        value[:top_dyno] = max_element(value[:dynos])
      end
    end

    def self.types
      {
        count_pending_messages: "GET /api/users/{user_id}/count_pending_messages",
        get_messages: "GET /api/users/{user_id}/get_messages",
        get_friends_progress: "GET /api/users/{user_id}/get_friends_progress",
        get_friends_score: "GET /api/users/{user_id}/get_friends_score",
        post_user: "POST /api/users/{user_id}",
        get_user: "GET /api/users/{user_id}",
      }
    end

    private

    def initialize_store
      self.class.types.each do |_, value|
        @request_info_store[value] = {
          hits_count: 0,
          response_time: [],
          dynos: {},
          median: nil,
          mean: nil,
          mode: [],
          top_dyno: nil
        }
      end
    end

    def build_store
      File.open(@file_path, 'r').each do |log|
        parser = log.scan(HEROKU_INFO_LOGS_REGEXP)[0]

        if parser.nil?
          puts "Can't parse: #{log}\n\n"
          next
        end

        parse = {
          timestamp:  parser[0],
          name:       parser[1],
          at:         parser[2],
          method:     parser[3],
          path:       parser[4],
          host:       parser[5],
          fwd:        parser[6],
          dyno:       parser[7],
          connect:    parser[8],
          service:    parser[9],
          status:     parser[10],
          bytes:      parser[11]
        }

        store_item(parse)
      end

    rescue Errno::ENOENT
      puts "Can't open this file: #{@file_path}"
    end

    def store_item(data)
      path = data[:path]

      if /api\/users\/[0-9a-zA-Z-]+\/count_pending_messages/ =~ path
        update_store(:count_pending_messages, data)
      elsif /api\/users\/[0-9a-zA-Z-]+\/get_messages/ =~ path
        update_store(:get_messages, data)
      elsif /api\/users\/[0-9a-zA-Z-]+\/get_friends_progress/ =~ path
        update_store(:get_friends_progress, data)
      elsif /api\/users\/[0-9a-zA-Z-]+\/get_friends_score/ =~ path
        update_store(:get_friends_score, data)
      elsif /api\/users\/[0-9a-zA-Z-]+/ =~ path
        if data[:method] == 'POST'
          update_store(:post_user, data)
        elsif data[:method] == 'GET'
          update_store(:get_user, data)
        end
      end
    end

    def update_store(type, data)
      key = self.class.types[type]
      response_time = data[:connect].to_i + data[:service].to_i
      dynos = @request_info_store[key][:dynos]

      @request_info_store[key][:hits_count] += 1
      @request_info_store[key][:response_time] << response_time

      if dynos.has_key?(data[:dyno].to_sym)
        dynos[data[:dyno].to_sym] += 1
      else
        dynos[data[:dyno].to_sym] = 1
      end
    end

    def max_element(hash)
      max_element = hash.select { |k, v| k if v == hash.values.max }.keys.map(&:to_s)
      return max_element.first if max_element.size == 1
      max_element
    end
  end
end
